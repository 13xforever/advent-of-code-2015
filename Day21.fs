namespace AdventOfCode2015.Tests
open NUnit.Framework
open System

type ItemStats = {
    name: string
    cost: int
    dmg: int
    def: int
}
type CharStats = {
    hp: int
    mp: int
    dmg: int
    def: int
}
type CharType =
    | Hero = 0
    | Boss = 1
type Equipment = {
    w: ItemStats
    a: ItemStats
    r1: ItemStats
    r2: ItemStats
    cost: int
    dmg: int
    def: int
}

type ``Day 21: RPG Simulator 20XX``() = 

    let sim (hero: CharStats) (boss: CharStats) =
        let h2bdmg = max 1 (hero.dmg - boss.def)
        let b2hdmg = max 1 (boss.dmg - hero.def)
        let h2brnds = boss.hp / h2bdmg + (if boss.hp % h2bdmg = 0 then 0 else 1)
        let b2hrnds = hero.hp / b2hdmg + (if hero.hp % b2hdmg = 0 then 0 else 1)
        if h2brnds > b2hrnds then CharType.Boss else CharType.Hero

    let strToItem (l: string) =
        let valueSeparator = [| ' '; '\t' |]
        let p = l.Split(valueSeparator, StringSplitOptions.RemoveEmptyEntries)
        match p with
        | [| n; c; dmg; def |] -> { name = n; cost = int c; dmg = int dmg; def = int def }
        | [| n; m; c; dmg; def |] -> { name = n + " " + m; cost = int c; dmg = int dmg; def = int def }
        | _ -> failwithf "Invalid item format: %s" l

    let nothing = { name = "nothing"; cost = 0; dmg = 0; def = 0 }
    let naked = { w = nothing; a = nothing; r1 = nothing; r2 = nothing; cost = 0; dmg = 0; def = 0 }

    let parseList (s: string) =
        let lineSeparator = [| '\r'; '\n' |]
        s.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries)
        |> Seq.ofArray
        |> Seq.map strToItem
        |> List.ofSeq

    let calcStats (e: Equipment) =
        { e with cost = e.w.cost + e.a.cost + e.r1.cost + e.r2.cost
                 dmg  = e.w.dmg  + e.a.dmg  + e.r1.dmg  + e.r2.dmg
                 def  = e.w.def  + e.a.def  + e.r1.def  + e.r2.def }

    let tailAfter (l: 't list) (e: 't) =
        List.skipWhile (fun i -> i <> e) l
        |> List.skip 1

    let chooseEquipment weapons armors rings boss victor selector =
        let weaponList = parseList weapons
        let armorList = nothing::(parseList armors)
        let ringList = nothing::(parseList rings)
        seq { for w in weaponList do
                for a in armorList do
                    //printfn "Considering rings for %s with %s..." w.name a.name
                    for r1 in ringList do
                        for r2 in (tailAfter ringList r1) do
                            let e = calcStats { naked with w = w; a = a; r1 = r1; r2 = r2 }
                            if (sim { hp = 100; mp = 0; dmg = e.dmg; def = e.def } boss) = victor then
                                yield e
        }
        |> selector (fun e -> e.cost)

    [<TestCase(8, 5, 5, 12, 7, 2, ExpectedResult = CharType.Hero)>]
    [<TestCase(8, 5, 5, 13, 7, 2, ExpectedResult = CharType.Boss)>]
    member __.SimTest hhp hdmg hdef bhp bdmg bdef =
        int (sim {hp = hhp; mp = 0; dmg = hdmg; def = hdef} {hp = bhp; mp = 0; dmg = bdmg; def = bdef})

    [<TestCase(100, 8, 2, ExpectedResult = 91)>]
    member this.``Part 1 `` hp dmg def =
        let result = chooseEquipment this.WeaponList this.ArmorList this.RingList { hp = hp; mp = 0; dmg = dmg; def = def } CharType.Hero Seq.minBy
        printfn "Min equipment to win: %A" result
        result.cost

    [<TestCase(100, 8, 2, ExpectedResult = 158)>]
    member this.``Part 2 `` hp dmg def =
        let result = chooseEquipment this.WeaponList this.ArmorList this.RingList { hp = hp; mp = 0; dmg = dmg; def = def } CharType.Boss Seq.maxBy
        printfn "Max equipment to lose: %A" result
        result.cost

    member __.WeaponList = """
Dagger        8     4       0
Shortsword   10     5       0
Warhammer    25     6       0
Longsword    40     7       0
Greataxe     74     8       0
"""

    member __.ArmorList = """
Leather      13     0       1
Chainmail    31     0       2
Splintmail   53     0       3
Bandedmail   75     0       4
Platemail   102     0       5
"""
    member __.RingList = """
Damage +1    25     1       0
Damage +2    50     2       0
Damage +3   100     3       0
Defense +1   20     0       1
Defense +2   40     0       2
Defense +3   80     0       3
"""
