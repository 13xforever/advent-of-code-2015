namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type Formula = string list
type Transp = string * string

type ``Day 19: Medicine for Rudolph``() = 

    let lineSeparator = [| '\r'; '\n' |]
    let transpSeparator = [| ' '; '\t'; '='; '>' |]

    let parseFormula (s: string) =
        let rec parse (r: Formula) (c: string) (str: string) =
            if str.Length = 0 then
                c::r
            else
                match str.[0] with
                | cc when (cc >= 'A' && cc <= 'Z') || (cc >= '0' && cc <= '9') -> parse (c::r) (str.[0..0]) (str.[1..])
                | _ -> parse r (c + str.[0..0]) (str.[1..])
        parse [] (s.[0..0]) (s.[1..])
        |> List.rev

    let parseTransp (s: string) =
        s.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries)
        |> Seq.ofArray
        |> Seq.map (fun l -> l.Split(transpSeparator, StringSplitOptions.RemoveEmptyEntries)
                             |> (fun lp -> (lp.[0], lp.[1])))
        |> List.ofSeq

    let rec formulaToStr (s: string) (f: Formula) =
        match f with
        | [] -> s
        | h::t -> formulaToStr (s+h) t

    let rec generateSubs (f: Formula) (cf: string) (s: Transp list) (r: Set<string>) =
        match f with
        | [] -> r
        | h::t -> seq { for (fr, t) in s do
                            if h = fr then yield t else () }
                  |> Seq.fold (fun (acc: Set<string>) e -> acc.Add(formulaToStr (cf + e) t)) r
                  |> generateSubs t (cf+h) s

    let rec reduce (minSteps: int) (steps: int) (f: string) (rt: Transp list) =
        let rec replaceRec (f: string) (tf: string) (tt: string) (sIdx: int) (r: string list) =
            match f.IndexOf(tf, sIdx) with
            | -1 -> r
            | idx -> let replaced = f.[0..(idx-1)] + tt + f.[(idx+tf.Length)..]
                     let r = replaced::r
                     replaceRec f tf tt (idx+1) r
        if steps >= minSteps || (f.Length > 1 && f.IndexOf("e") > -1) then
            minSteps
        else
            seq { yield minSteps
                  let mutable localMinSteps = minSteps
                  for (tf, tt) in rt do
                    let reducedFs = replaceRec f tf tt 0 []
                    match reducedFs with
                    | [] -> yield localMinSteps
                    | _ -> for reducedF in reducedFs do
                                let steps = steps + 1
                                match reducedF with
                                | "e" -> localMinSteps <- (if steps < localMinSteps then steps else localMinSteps)
                                         if (localMinSteps < minSteps) then
                                            yield localMinSteps
                                         else
                                            ()
                                | _ -> localMinSteps <- (reduce localMinSteps steps reducedF rt)
                                       if (localMinSteps < minSteps) then yield localMinSteps else ()
                }
            //this is such a hack
            |> Seq.where (fun e -> e < Int32.MaxValue)
            |> Seq.head

    let countUniqueSubstitutions (subs: string) (input: string) =
        let transp = parseTransp subs
        let formula = parseFormula input
        let result = generateSubs formula "" transp Set.empty
        result.Count
    
    let findMinReduction (subs: string) (input: string) =
        let reverseTransp = parseTransp subs
                            |> List.map (fun (f, s) -> (s, f))
                            |> List.sortByDescending (fun (ef, _) -> ef.Length)
        reduce Int32.MaxValue 0 input reverseTransp

    [<TestCase("test", "HOH", ExpectedResult = 4)>]
    [<TestCase("test", "HOHOHO", ExpectedResult = 7)>]
    [<TestCase("test", "H2O", ExpectedResult = 3)>]
    [<TestCase("real", "real", ExpectedResult = 518)>]
    member this.``Part 1 `` substitutions input =
        let subs = match substitutions with
                   | "real" -> this.RealInputReplacements
                   | _ -> this.TestInputReplacements
        match input with
        | "real" -> countUniqueSubstitutions subs this.RealInputStart
        | _ -> countUniqueSubstitutions subs input

    [<TestCase("test", "HOH", ExpectedResult = 3)>]
    [<TestCase("test", "HOHOHO", ExpectedResult = 6)>]
    [<TestCase("real", "real", ExpectedResult = 200)>]
    member this.``Part 2 `` substitutions input =
        let subs = match substitutions with
                   | "real" -> this.RealInputReplacements
                   | _ -> this.TestInputReplacements
        match input with
        | "real" -> findMinReduction subs this.RealInputStart
        | _ -> findMinReduction subs input

    member __.TestInputReplacements = """
e => H
e => O
H => HO
H => OH
O => HH
"""
    member __.RealInputStart =
        "CRnSiRnCaPTiMgYCaPTiRnFArSiThFArCaSiThSiThPBCaCaSiRnSiRnTiTiMgArPBCaPMgYPTiRnFArFArCaSiRnBPMgArPRnCaPTiRnFArCaSiThCaCaFArPBCaCaPTiTiRnFArCaSiRnSiAlYSiThRnFArArCaSiRnBFArCaCaSiRnSiThCaCaCaFYCaPTiBCaSiThCaSiThPMgArSiRnCaPBFYCaCaFArCaCaCaCaSiThCaSiRnPRnFArPBSiThPRnFArSiRnMgArCaFYFArCaSiRnSiAlArTiTiTiTiTiTiTiRnPMgArPTiTiTiBSiRnSiAlArTiTiRnPMgArCaFYBPBPTiRnSiRnMgArSiThCaFArCaSiThFArPRnFArCaSiRnTiBSiThSiRnSiAlYCaFArPRnFArSiThCaFArCaCaSiThCaCaCaSiRnPRnCaFArFYPMgArCaPBCaPBSiRnFYPBCaFArCaSiAl"

    member __.RealInputReplacements = """
Al => ThF
Al => ThRnFAr
B => BCa
B => TiB
B => TiRnFAr
Ca => CaCa
Ca => PB
Ca => PRnFAr
Ca => SiRnFYFAr
Ca => SiRnMgAr
Ca => SiTh
F => CaF
F => PMg
F => SiAl
H => CRnAlAr
H => CRnFYFYFAr
H => CRnFYMgAr
H => CRnMgYFAr
H => HCa
H => NRnFYFAr
H => NRnMgAr
H => NTh
H => OB
H => ORnFAr
Mg => BF
Mg => TiMg
N => CRnFAr
N => HSi
O => CRnFYFAr
O => CRnMgAr
O => HP
O => NRnFAr
O => OTi
P => CaP
P => PTi
P => SiRnFAr
Si => CaSi
Th => ThCa
Ti => BP
Ti => TiTi
e => HF
e => NAl
e => OMg
"""
