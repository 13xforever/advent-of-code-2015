﻿namespace AdventOfCode2015.Tests
open System.Security.Cryptography
open System.Text
open NUnit.Framework

type ZeroTest = array<byte> -> bool

type ``Day 04: The Ideal Stocking Stuffer``() = 

    let testFiveZeroes (hash: array<byte>) =
        let zeroPrefix = Array.forall (fun b -> b = byte 0) hash.[0..1]
        let fifthZero = hash.[2] < byte 0x10
        zeroPrefix && fifthZero

    let testSixZeroes (hash: array<byte>) =
        Array.forall (fun b -> b = byte 0) hash.[0..2]
    
    let calcHash (input: array<byte>) =
        use md5 = MD5.Create()
        md5.ComputeHash input

    let rec hash (zeroTest: ZeroTest) (seed: string) (num: int) =
        let hashCandidate = seed + (string num)
                            |> Encoding.ASCII.GetBytes
                            |> calcHash
        if (zeroTest hashCandidate) then
            num
        else
            hash zeroTest seed (num + 1)

    [<TestCase("abcdef", ExpectedResult = 609043)>]
    [<TestCase("pqrstuv", ExpectedResult = 1048970)>]
    [<TestCase("bgvyzdsv", ExpectedResult = 254575)>]
    member __.``Part 1 `` seed =
        hash testFiveZeroes seed 1

    [<TestCase("bgvyzdsv", ExpectedResult = 1038736)>]
    member __.``Part 2 `` seed =
        hash testSixZeroes seed 1
