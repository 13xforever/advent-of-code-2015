namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type Registers = Map<string, int64>
type Interpret = TvmState -> TvmState
and JitCache = Map<string, Interpret>
and TvmState = {
    program: string[]
    ip: int
    regs: Registers
    ops: Interpret[]
}

type ``Day 23: Opening the Turing Lock``() = 

    let (|Integer|_|) (str: string) =
        let mutable intvalue = 0L
        if Int64.TryParse(str, &intvalue) then Some(intvalue) else None

    let getValue (s: string) (r: Registers) =
        match s with
        | Integer i -> i
        | _ -> match Map.tryFind s r with
               | Some(v) -> v
               | None -> 0L

    let compileLine (jc: JitCache) (instr: string * string[]) =
        let (l, p) = instr
        if jc.ContainsKey l then
            jc
        else
            let reg = p.[1]
            let f: Interpret = match p.[0] with
                               | "hlf" -> fun s -> let v = (getValue reg s.regs) / 2L
                                                   { s with ip = s.ip + 1
                                                            regs = s.regs.Add(reg, v) }
                               | "tpl" -> fun s -> let v = (getValue reg s.regs) * 3L
                                                   { s with ip = s.ip + 1
                                                            regs = s.regs.Add(reg, v) }
                               | "inc" -> fun s -> let v = (getValue reg s.regs) + 1L
                                                   { s with ip = s.ip + 1
                                                            regs = s.regs.Add(reg, v) }
                               | "set" -> let v = getValue p.[2] Map.empty
                                          fun s -> { s with ip = s.ip + 1
                                                            regs = s.regs.Add(reg, v) }
                               | "jmp" -> let off = int (getValue reg Map.empty)
                                          fun s -> { s with ip = s.ip + off }
                               | "jie" -> let off = int (getValue p.[2] Map.empty)
                                          fun s -> let v = getValue reg s.regs
                                                   let offset = if v &&& 1L = 0L then off else 1
                                                   { s with ip = s.ip + offset }
                               | "jio" -> let off = int (getValue p.[2] Map.empty)
                                          fun s -> let v = getValue reg s.regs
                                                   let offset = if v = 1L then off else 1
                                                   { s with ip = s.ip + offset }
                               | _ -> failwithf "Unknown instruction %s" l
            jc.Add(l, f)

    let compile (input: string) =
        let lineSeparator = [| '\r'; '\n' |]
        let valueSeparator = [| ' '; '\t'; ',' |]
        let program = input.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries)
        let jitCache = program
                       |> Seq.ofArray
                       |> Seq.map (fun l -> l, l.Split(valueSeparator, StringSplitOptions.RemoveEmptyEntries))
                       |> Seq.fold (compileLine) Map.empty
        let ops = Array.map (fun l -> jitCache.[l]) program
        { program = program
          ip = 0
          regs = Map.empty
          ops = ops }

    let rec executeRec (s: TvmState) =
        if s.ip < 0 || s.ip >= s.program.Length then
            s
        else
            executeRec (s.ops.[s.ip](s))

    let execute (input: string) = executeRec (compile input)

    let execute1 (input: string) =
        let vm = compile input
        let vm = { vm with regs = vm.regs.Add("a", 1L) }
        executeRec vm

    let decodedExec (v: int64) =
        let rec decodeRec c (v: int64) =
            match v with
            | 1L -> c
            | _  -> match v &&& 1L with
                    | 1L -> decodeRec (c + 1) (v * 3L + 1L)
                    | _  -> decodeRec (c + 1) (v / 2L)
        int64 (decodeRec 0 v)

    [<TestCase("test", "a", ExpectedResult = 2)>]
    [<TestCase("real", "b", ExpectedResult = 170)>]
    [<TestCase("opt", "b", ExpectedResult = 170)>]
    [<TestCase("rec", "b", ExpectedResult = 170)>]
    member this.``Part 1 `` input resultReg =
        match input with
        | "test" -> let result = execute this.TestInput
                    result.regs.[resultReg]
        | "real" -> let result = execute this.RealInput
                    result.regs.[resultReg]
        | "opt"  -> let result = execute this.OptimizedRealInput
                    result.regs.[resultReg]
        | _ -> decodedExec 4591L

    [<TestCase("test", "a", ExpectedResult = 7)>]
    [<TestCase("real", "b", ExpectedResult = 247)>]
    [<TestCase("opt", "b", ExpectedResult = 247)>]
    [<TestCase("rec", "b", ExpectedResult = 247)>]
    member this.``Part 2 `` input resultReg =
        match input with
        | "test" -> let result = execute1 this.TestInput
                    result.regs.[resultReg]
        | "real" -> let result = execute1 this.RealInput
                    result.regs.[resultReg]
        | "opt"  -> let result = execute1 this.OptimizedRealInput
                    result.regs.[resultReg]
        | _ -> decodedExec 113383L

    member __.TestInput = """
inc a
jio a, +2
tpl a
inc a
"""

    member __.RealInput = """
jio a, +16
inc a
inc a
tpl a
tpl a
tpl a
inc a
inc a
tpl a
inc a
inc a
tpl a
tpl a
tpl a
inc a
jmp +23
tpl a
inc a
inc a
tpl a
inc a
inc a
tpl a
tpl a
inc a
inc a
tpl a
inc a
tpl a
inc a
tpl a
inc a
inc a
tpl a
inc a
tpl a
tpl a
inc a
jio a, +8
inc b
jie a, +4
tpl a
inc a
jmp +2
hlf a
jmp -7
"""

    member __.OptimizedRealInput = """
jio a, +3
set a, 4591
jmp +2
set a, 113383
jio a, +8
inc b
jie a, +4
tpl a
inc a
jmp -5
hlf a
jmp -7
"""