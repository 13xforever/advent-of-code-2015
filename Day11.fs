namespace AdventOfCode2015.Tests
open NUnit.Framework

type Password = char list

type ``Day 11: Corporate Policy``() = 

    let rec fill (r: Password) (p: Password) =
        match p with
        | [] -> r
        | _::t -> fill ('a'::r) t

    let rec sanitize (r: Password) (p: Password) =
        match p with
        | [] -> r
        | h::t -> match h with
                  | 'i' -> fill ('j'::r) t
                  | 'l' -> fill ('m'::r) t
                  | 'o' -> fill ('p'::r) t
                  | c   -> sanitize (c::r) t

    let strToList (s: string) =
        s.ToCharArray()
        |> List.ofArray
        |> sanitize []

    let listToStr (l: char list) =
        l
        |> List.rev
        |> Array.ofList
        |> (fun arr -> new string(arr))

    let inc (c: char) = char ((uint16 c) + 1us)

    let rec increment (p: Password) =
        match p with
        | [] -> ['a']
        | h::t -> match h with
                  | 'h' -> 'j'::t
                  | 'k' -> 'm'::t
                  | 'n' -> 'p'::t
                  | 'z' -> 'a'::(increment t)
                  | c -> (inc c)::t

    let rec isAcceptableRec (conseq: bool) (same: Set<char>) (p: Password) =
        let conseq = conseq || match p with
                               | [] -> false
                               | h1::t1 -> match t1 with
                                           | [] -> false
                                           | h2::t2 when h1 = inc h2 -> match t2 with
                                                                        | [] -> false
                                                                        | h3::_ when h2 = inc h3 -> true
                                                                        | _ -> false
                                           | _ -> false
        let same = match p with
                   | [] -> same
                   | h1::t1 -> match t1 with
                               | [] -> same
                               | h2::_ when h1 = h2 -> same.Add h1
                               | _ -> same
        match (conseq, same.Count > 1) with
        | (true, true) -> true
        | _ -> match p with
               | [] -> false
               | _::t -> isAcceptableRec conseq same t

    let isAcceptable (p: Password) = isAcceptableRec false Set.empty p

    let rec generateRec (p: Password) =
        let next = increment p
        match isAcceptable next with
        | true -> next
        | false -> generateRec next

    let generate (p: string) = generateRec (strToList p) |> listToStr

    [<TestCase("abcdefgh", ExpectedResult = "abcdefgj")>]
    [<TestCase("abcdefgz", ExpectedResult = "abcdefha")>]
    [<TestCase("abcdefzz", ExpectedResult = "abcdegaa")>]
    [<TestCase("abcdehzz", ExpectedResult = "abcdejaa")>]
    member __.``Password increment `` input =
        increment (strToList input) |> listToStr

    [<TestCase("abcdefgh", ExpectedResult = false)>]
    [<TestCase("abbceffg", ExpectedResult = false)>]
    [<TestCase("abbcegjk", ExpectedResult = false)>]
    [<TestCase("ccccccba", ExpectedResult = false)>]
    [<TestCase("abcccccc", ExpectedResult = false)>]
    [<TestCase("abccccdd", ExpectedResult = true)>]
    [<TestCase("abcdffaa", ExpectedResult = true)>]
    [<TestCase("ghjaabcc", ExpectedResult = true)>]
    member __.``Is password acceptable `` input =
        isAcceptable (strToList input)

    [<TestCase("abcdefgh", ExpectedResult = "abcdffaa")>]
    [<TestCase("ghijklmn", ExpectedResult = "ghjaabcc")>]
    [<TestCase("vzbxkghb", ExpectedResult = "vzbxxyzz")>]
    member __.``Part 1 `` input =
        generate input

    [<TestCase("vzbxkghb", ExpectedResult = "vzcaabcc")>]
    member __.``Part 2 `` input =
        generate input |> generate
