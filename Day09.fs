﻿namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type NodeName = string
type Vertex = {
    nodeA: NodeName
    nodeB: NodeName
    len: int
}
type Graph = Map<NodeName, list<Vertex>>
type Comparer = int -> int -> int

type ``Day 09: All in a Single Night``() = 

    let valueSeparator = [| ' '; '\t'; '=' |]

    let parseVertex (l: string) =
        match l.Split(valueSeparator, StringSplitOptions.RemoveEmptyEntries) with
        | [| n1; _; n2; l |] -> {nodeA = n1; nodeB = n2; len = int l}
        | _ -> failwithf "Invalid node format %s" l

    let parseInput (s: string) =
        let lineSeparator = [| '\r'; '\n' |]
        s.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries)
        |> Seq.ofArray
        |> Seq.map parseVertex
        |> List.ofSeq
    
    let updateGraph (v: Vertex) (g: Graph) =
        let k = v.nodeA
        let v = if (g.ContainsKey k) then v::g.[k] else [v]
        g.Add(k, v)

    let rec buildGraph (g: Graph) (vertices: list<Vertex>) =
        match vertices with
        | [] -> g
        | v::t -> let vv = {nodeA = v.nodeB; nodeB = v.nodeA; len = v.len}
                  let g = updateGraph v g
                  let g = updateGraph vv g
                  buildGraph g t

    let rec minPathFromVertex (initValue: int) (comp: Comparer) (len: int) (minLen: int) (nn: NodeName) (g: Graph) =
        let gg = g.Remove nn
        let nextVertices = g.[nn] |> List.where (fun v -> (gg.ContainsKey v.nodeB))
        match nextVertices with
        | [] -> comp len minLen
        | _ -> nextVertices
               |> List.map (fun vv -> minPathFromVertex initValue comp (len+vv.len) minLen vv.nodeB gg)
               |> List.fold (comp) initValue

    let minPath (initValue: int) (comp: Comparer) (g: Graph) =
        g
        |> Map.toSeq
        |> Seq.map fst
        |> Seq.map (fun v -> minPathFromVertex initValue comp 0 initValue v g)
        |> Seq.fold (comp) initValue

    let findPath (input: string) (initValue: int) (comp: Comparer) =
        parseInput input
        |> buildGraph Map.empty
        |> minPath initValue comp

    [<TestCase("test", ExpectedResult = 605)>]
    [<TestCase("real", ExpectedResult = 251)>]
    member this.``Part 1 `` input =
        let input = match input with
                    | "test" -> this.TestInput
                    | _ -> this.RealInput
        findPath input Int32.MaxValue min

    [<TestCase("test", ExpectedResult = 982)>]
    [<TestCase("real", ExpectedResult = 898)>]
    member this.``Part 2 `` input =
        let input = match input with
                    | "test" -> this.TestInput
                    | _ -> this.RealInput
        findPath input 0 max

    member __.TestInput = """
London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141
"""

    member __.RealInput = """
Tristram to AlphaCentauri = 34
Tristram to Snowdin = 100
Tristram to Tambi = 63
Tristram to Faerun = 108
Tristram to Norrath = 111
Tristram to Straylight = 89
Tristram to Arbre = 132
AlphaCentauri to Snowdin = 4
AlphaCentauri to Tambi = 79
AlphaCentauri to Faerun = 44
AlphaCentauri to Norrath = 147
AlphaCentauri to Straylight = 133
AlphaCentauri to Arbre = 74
Snowdin to Tambi = 105
Snowdin to Faerun = 95
Snowdin to Norrath = 48
Snowdin to Straylight = 88
Snowdin to Arbre = 7
Tambi to Faerun = 68
Tambi to Norrath = 134
Tambi to Straylight = 107
Tambi to Arbre = 40
Faerun to Norrath = 11
Faerun to Straylight = 66
Faerun to Arbre = 144
Norrath to Straylight = 115
Norrath to Arbre = 135
Straylight to Arbre = 127
"""