namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type Bag = {
    items: Set<int>
    w: int
    q: uint64
}

type ``Day 24: It Hangs in the Balance``() = 

    let parseList (s: string) =
        let lineSeparator = [| '\r'; '\n' |]
        s.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries)
        |> Seq.ofArray
        |> Seq.map int
        |> List.ofSeq
        |> List.sortDescending

    let targetBagWeight (l: int list) (bc: int) =
        let s = List.sum l
        if s % bc <> 0 then
            failwithf "%i is not divisible by 3" s
        s / bc

    let findMinBag (l: int list) (t: int) (consumeEverything: bool) =
        let mutable minBag = { items = Set.ofList l
                               w = t
                               q = UInt64.MaxValue }
        let rec findMinBagRec (current: Bag) (all: Set<int>) =
            if current.items.Count > minBag.items.Count || current.q >= minBag.q then
                minBag
            else
                let remaining = all
                                |> Set.toList
                                |> List.sortDescending
                let s = seq { for i in remaining do
                                let current = { items = current.items.Add i
                                                w = current.w + i
                                                q = if current.q = 0UL then uint64 i else current.q * (uint64 i) }
                                if (current.w = t) && ((current.items.Count < minBag.items.Count) || ((current.items.Count = minBag.items.Count) && (current.q < minBag.q))) then
                                    printfn "\t New min bag: q=%i, %A" current.q current.items
                                    minBag <- current
                                    yield current
                                else if current.w < t then
                                    yield findMinBagRec current (all.Remove i)
                              yield minBag
                }
                if consumeEverything then
                    s |> Seq.iter ignore
                else
                    s |> Seq.head |> ignore
                minBag
        findMinBagRec { items = Set.empty; w = 0; q = 0UL } (Set.ofList l)
    
    let findMinQ (bc: int) (s: string) (consumeEverything: bool) =
        let lst = parseList s
        let target = targetBagWeight lst bc
        let result = findMinBag lst target consumeEverything
        printfn "Optimal bag: %A" result
        result.q

    [<TestCase("test", ExpectedResult = 99UL)>]
    [<TestCase("real", ExpectedResult = 11266889531UL)>]
    member this.``Part 1 `` input =
        findMinQ 3 (match input with
                    | "test" -> this.TestInput
                    | _ -> this.RealInput) false

    [<TestCase("test", ExpectedResult = 44UL)>]
    [<TestCase("real", ExpectedResult = 77387711UL)>]
    member this.``Part 2 `` input =
        findMinQ 4 (match input with
                    | "test" -> this.TestInput
                    | _ -> this.RealInput) true

    member __.TestInput = """
1
2
3
4
5
7
8
9
10
11
"""

    member __.RealInput = """
1
3
5
11
13
17
19
23
29
31
41
43
47
53
59
61
67
71
73
79
83
89
97
101
103
107
109
113
"""