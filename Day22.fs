namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type MagicEffect = {
    n: string
    cost: int
    t: int
    dmg: int
    def: int
    hp: int
    mp: int
}
type CharState = {
    n: CharStats
    b: CharStats
    e: MagicEffect list
}

type ``Day 22: Wizard Simulator 20XX``() = 

    let puff = { n = ""; cost = 0; t = 1; dmg = 0; def = 0; hp = 0; mp = 0; }
    let spellList = [
        { puff with n = "Magic Missile"; cost =  53;        dmg = 4 }
        { puff with n = "Drain";         cost =  73;        dmg = 2; hp = 2 }
        { puff with n = "Shield";        cost = 113; t = 6; def = 7 }
        { puff with n = "Poison";        cost = 173; t = 6; dmg = 3 }
        { puff with n = "Recharge";      cost = 229; t = 5; mp = 101 }
    ]

    let applyEffect (r: CharState) (eff: MagicEffect) =
        let buff = { hp = r.b.hp + eff.hp
                     mp = r.b.mp + eff.mp
                     dmg = r.b.dmg + eff.dmg
                     def = r.b.def + eff.def }
        { r with b = buff }

    let rec applyEffects (result: CharState) (effectList: MagicEffect list) =
        match effectList with
        | [] -> result
        | cur::t -> let eff = { cur with t = cur.t - 1 }
                    let r = { result with e = (if eff.t = 0 then result.e else eff::result.e) }
                    let r = applyEffect r eff
                    applyEffects r t

    let roundP (hero: CharState) (boss: CharStats) =
        if (hero.b.dmg = 0) then
            hero, boss
        else
            let bres = { boss with hp = boss.hp - (max 1 hero.b.dmg) }
            hero, bres

    let roundB (hero: CharState) (boss: CharStats) =
        let bres = if hero.b.dmg > 0 then
                        { boss with hp = boss.hp - (max 1 hero.b.dmg) }
                   else
                        boss
        let hero = { hero with n = { hero.n with mp = hero.b.mp } }
        if bres.hp <= 0 then
            hero, bres
        else
            let hres = { hero.n with hp = hero.b.hp - (max 1 (bres.dmg - hero.b.def)) }
            { hero with n = { hero.n with hp = hres.hp; mp = hres.mp }; b = hres }, bres

    let canUse (spell: MagicEffect) (effectList: MagicEffect list) =
        not (effectList |> List.exists (fun eff -> eff.cost = spell.cost))

    let chooseSpells hhp hmp bhp bdmg constDmg =
        let mutable minManaCost = Int32.MaxValue
        let mutable minChain = []
        let rec chooseSpellsRec (hero: CharState) (boss: CharStats) (manaCost: int) (chain: string list) =
            if (manaCost > minManaCost || hero.n.hp <= constDmg) then
                minManaCost, minChain
            else
                let hero = { hero with b = { hero.b with hp = hero.b.hp - constDmg } }
                let buffedHero = applyEffects { hero with e = [] } hero.e
                let (uph, b) = roundP buffedHero boss
                seq { for s in spellList do
                        if (b.hp <= 0) then
                            if manaCost < minManaCost then
                                minManaCost <- manaCost
                                minChain <- chain
                                printfn "\tNew chain: %i %A" manaCost (List.rev chain)
                            yield manaCost, chain
                        else if (s.cost <= uph.b.mp) && (canUse s uph.e) then
                            let manaCost = manaCost + s.cost
                            let chain = s.n::chain
                            let uph = applyEffects { uph with b = { uph.n with hp = uph.b.hp; mp = uph.b.mp - s.cost }; e = [] } (s::uph.e)
                            let (uph, b) = roundB uph b
                            if b.hp <= 0 then
                                if manaCost < minManaCost then
                                    minManaCost <- manaCost
                                    minChain <- chain
                                    printfn "\tNew chain: %i %A" manaCost (List.rev chain)
                                yield manaCost, chain
                            else if uph.n.hp <= 0 || uph.n.mp < 0 then
                                ()
                            else
                                yield chooseSpellsRec uph b manaCost chain
                }
                |> Seq.iter ignore
                (minManaCost, minChain)
        let heroBase = { hp = hhp; mp = hmp; dmg = 0; def = 0 }
        let hero = { n = heroBase
                     b = heroBase
                     e = [] }
        let boss = { hp = bhp; mp = 0; dmg = bdmg; def = 0 }
        chooseSpellsRec hero boss 0 []

    let printBeforePlayerTurn round hero (boss: CharStats) (spell: MagicEffect) constDmg =
        let effects = hero.e |> List.map (fun e -> e.n + " " + (string e.t))
        match constDmg with
        | 0 -> printfn "%i\t|>\t%i\t%i\t%i\t%i\t%i\t%A <%s>" round hero.b.hp hero.b.def hero.b.mp hero.b.dmg boss.hp effects spell.n |> ignore
        | _ -> printfn "%i\t|>\t%i-%i\t%i\t%i\t%i\t%i\t%A <%s>" round (hero.b.hp + constDmg) constDmg hero.b.def hero.b.mp hero.b.dmg boss.hp effects spell.n |> ignore

    let printAfterPlayerTurn hero (boss: CharStats) spell =
        let effects = (hero.e @ [ spell ]) |> List.map (fun e -> e.n + " " + (string e.t))
        printfn "\t|<\t%i\t%i\t%i\t%i\t%i\t%A" hero.b.hp hero.b.def (hero.b.mp - spell.cost) hero.b.dmg boss.hp effects |> ignore

    let printBeforeBossTurn hero (boss: CharStats) =
        let effects = hero.e |> List.map (fun e -> e.n + " " + (string e.t))
        printfn "\t*>\t%i\t%i\t%i\t%i\t%i\t%A <!%i>" hero.b.hp hero.b.def hero.b.mp hero.b.dmg boss.hp effects boss.dmg |> ignore

    let printAfterBossTurn hero (boss: CharStats) =
        let effects = hero.e |> List.map (fun e -> e.n + " " + (string e.t))
        printfn "\t*<\t%i\t%i\t%i\t%i\t%i\t%A" hero.b.hp hero.b.def hero.b.mp hero.b.dmg boss.hp effects |> ignore

    let replay hhp hmp bhp bdmg spellNameList constDmg =
        let rec replayRec (hero: CharState) (boss: CharStats) (chain: string list) round =
            match chain with
            | [] -> hero, boss
            | s::t -> let buffedHero = applyEffects { hero with b = { hero.b with hp = hero.b.hp - constDmg }; e = [] } hero.e
                      let spell = spellList |> List.where (fun sp -> sp.n = s) |> List.head
                      printBeforePlayerTurn round buffedHero boss spell constDmg
                      let (uph, b) = roundP buffedHero boss
                      printAfterPlayerTurn uph b spell
                      let uph = applyEffects { uph with b = { uph.n with hp = uph.b.hp; mp = uph.b.mp - spell.cost }; e = [] } (spell::uph.e)
                      printBeforeBossTurn uph b
                      let (uph, b) = roundB uph b
                      printAfterBossTurn uph b
                      replayRec uph b t (round + 1)

        let heroBase = { hp = hhp; mp = hmp; dmg = 0; def = 0 }
        let hero = { n = heroBase
                     b = heroBase
                     e = [] }
        let boss = { hp = bhp; mp = 0; dmg = bdmg; def = 0 }
        printfn "R\tT\tHP\tDEF\tMP\tDMG\tBHP\tEffects"
        replayRec hero boss spellNameList 1

    [<TestCase(10, 250, 13, 8, "Poison, Magic Missile", ExpectedResult = "2 24 0")>]
    [<TestCase(10, 250, 14, 8, "Recharge, Shield, Drain, Poison, Magic Missile", ExpectedResult = "1 114 -1")>]
    member __.Replay hhp hmp bhp bdmg (spells: string) =
        let valueSeparator = [| ',' |]
        let spellList = spells.Split(valueSeparator) |> List.ofArray |> List.map (fun s -> s.Trim())
        let (hero, boss) = replay hhp hmp bhp bdmg spellList 0
        String.Format("{0} {1} {2}", hero.n.hp, hero.n.mp, boss.hp)

    [<TestCase(50, 500, 71, 10, ExpectedResult = 1824)>]
    member __.``Part 1 `` hhp hmp bhp bdmg =
        let (result, chain) = chooseSpells hhp hmp bhp bdmg 0
        let chain = List.rev chain
        printfn "\nMin spell chain to win: %A" chain
        replay hhp hmp bhp bdmg chain 0 |> ignore
        result

    [<TestCase(50, 500, 71, 10, ExpectedResult = 1937)>]
    member __.``Part 2 `` hhp hmp bhp bdmg =
        let (result, chain) = chooseSpells hhp hmp bhp bdmg 1
        let chain = List.rev chain
        printfn "\nMin spell chain to win: %A" chain
        replay hhp hmp bhp bdmg chain 1 |> ignore
        result

