﻿namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type DeerStat = {
    name: string
    speed: int
    flyTime: int
    restTime: int
}
type Scores = Map<string, int>

type ``Day 14: Reindeer Olympics``() = 

    let parseDeerStat (l: string) =
        let valueSeparator = [| ' ' |]
        match l.Split(valueSeparator, StringSplitOptions.RemoveEmptyEntries) with
        | [| name; _; _; spd; _; _; flyTime; _; _; _; _; _; _; restTime; _ |] -> { name = name; speed = int spd; flyTime = int flyTime; restTime = int restTime}
        | _ -> failwithf "Invalid input line %s" l

    let parse (input: string) =
        let lineSeparator = [| '\r'; '\n' |]
        input.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries)
        |> Seq.ofArray
        |> Seq.map parseDeerStat
        |> List.ofSeq
    
    let getDistance (d: DeerStat) (t: int) =
        let cycleTime = d.flyTime + d.restTime
        let cycleCount = t / cycleTime
        let remT = t % cycleTime
        let remFlyTime = min remT d.flyTime
        let totalDistance = d.speed * (cycleCount * d.flyTime + remFlyTime)
        totalDistance
 
    let allMax (acc: int * DeerStat list) (el: DeerStat * int) =
        let (maxD, maxL) = acc
        match el with
        | (d, dist) when dist > maxD -> (dist, [d])
        | (d, dist) when dist = maxD -> (dist, d::maxL)
        | _ -> acc
    
    let updateScore (acc: Scores) (el: DeerStat) =
        if acc.ContainsKey el.name then
            acc.Add(el.name, acc.[el.name]+1)
        else
            acc.Add(el.name, 1)

    let updateScores (deerStats: DeerStat list) (s: Scores) (t: int) =
        seq { for d in deerStats do
                yield (d, getDistance d t) }
         |> Seq.fold (allMax) (0, [])
         |> snd
         |> List.fold (updateScore) s

    let getLongestDistance (input: string) (t: int) =
        let deerStats = parse input
        seq { for d in deerStats do
                yield (d, getDistance d t) }
        |> Seq.maxBy snd
        |> snd

    let getBestScore (input: string) (t: int) =
        let deerStats = parse input
        let rec getScore (t: int) (s: Scores) =
            match t with
            | 0 -> s
            | _ -> let s = updateScores deerStats s t
                   getScore (t-1) s
        let results = getScore t Map.empty
        let bestDeer = Map.toSeq results
                       |> Seq.maxBy snd
        snd bestDeer

    [<TestCase("test", 1000, ExpectedResult = 1120)>]
    [<TestCase("real", 2503, ExpectedResult = 2660)>]
    member this.``Part 1 `` input time =
        match input with
        | "test" -> getLongestDistance this.TestInput time
        | _ -> getLongestDistance this.RealInput time

    [<TestCase("test", 1000, ExpectedResult = 689)>]
    [<TestCase("real", 2503, ExpectedResult = 1256)>]
    member this.``Part 2 `` input time =
        match input with
        | "test" -> getBestScore this.TestInput time
        | _ -> getBestScore this.RealInput time

    member __.TestInput = """
Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
"""

    member __.RealInput = """
Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.
Rudolph can fly 3 km/s for 15 seconds, but then must rest for 28 seconds.
Donner can fly 19 km/s for 9 seconds, but then must rest for 164 seconds.
Blitzen can fly 19 km/s for 9 seconds, but then must rest for 158 seconds.
Comet can fly 13 km/s for 7 seconds, but then must rest for 82 seconds.
Cupid can fly 25 km/s for 6 seconds, but then must rest for 145 seconds.
Dasher can fly 14 km/s for 3 seconds, but then must rest for 38 seconds.
Dancer can fly 3 km/s for 16 seconds, but then must rest for 37 seconds.
Prancer can fly 25 km/s for 6 seconds, but then must rest for 143 seconds.
"""