namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type ``Day 25: Let It Snow``() = 

    let rowStartNumber row =
        if row = 1 then 1 else (row - 1) * ((row - 1) + 1) / 2 + 1

    let getIdx row col =
        let hr = row + col - 1
        let rs = rowStartNumber hr
        let idx = rs + col - 1
        printfn "hr: %i, rs: %i, idx: %i" hr rs idx
        idx

    let generateCode row col =
        let rec genCodeN n seed =
            match n with
            | 1 -> seed
            | _ -> genCodeN (n - 1) ((seed * 252533UL) % 33554393UL)
        let n = getIdx row col
        genCodeN n 20151125UL

    [<TestCase(1, ExpectedResult = 1)>]
    [<TestCase(2, ExpectedResult = 2)>]
    [<TestCase(3, ExpectedResult = 4)>]
    [<TestCase(4, ExpectedResult = 7)>]
    [<TestCase(5, ExpectedResult = 11)>]
    [<TestCase(6, ExpectedResult = 16)>]
    member __.``Get Row Start Number `` row =
        rowStartNumber row

    [<TestCase(1, 1, ExpectedResult = 1)>]
    [<TestCase(1, 6, ExpectedResult = 21)>]
    [<TestCase(6, 1, ExpectedResult = 16)>]
    [<TestCase(2, 4, ExpectedResult = 14)>]
    member __.``Get Index `` row column =
        getIdx row column

    [<TestCase(1, 1, ExpectedResult = 20151125UL)>]
    [<TestCase(1, 6, ExpectedResult = 33511524UL)>]
    [<TestCase(6, 1, ExpectedResult = 33071741UL)>]
    [<TestCase(6, 6, ExpectedResult = 27995004UL)>]
    [<TestCase(2981, 3075, ExpectedResult = 9132360UL)>]
    member __.``Part 1 `` row column =
        generateCode row column
