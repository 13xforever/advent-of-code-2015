What is this?
=============

This is my attempt at [Advent of Code 2015](https://adventofcode.com/2015) using F#.

How do I run it?
================

First, you need to have [.NET Core SDK 2.0](https://github.com/dotnet/core) or newer.

Then you simply run

```
$ dotnet build
$ dotnet test
```

This repo also has solution for Visual Studio 2017, and workspace for Visual Studio Code (including configured Build and Test commands).