﻿namespace AdventOfCode2015.Tests
open NUnit.Framework

type ``Day 10: Elves Look, Elves Say``() = 

    let strToList (s: string) = List.ofArray (s.ToCharArray())

    let listToStr (l: char list) = new string(Array.ofList l)

    let concat (o: char list) (count: int) (c: char) =
        let result = (string count).ToCharArray()
                     |> List.ofArray
                     |> List.fold (fun l c -> c::l) o
        c::result

    let rec look (c: char) (count: int) (i: char list) (o: char list) =
        match i with
        | [] -> concat o count c |> List.rev
        | h::t -> match h with
                  | m when m = c -> look c (count+1) t o
                  | w -> look w 1 t (concat o count c)
    
    let rec lookAndSay (s: char list) (c: int) =
        let s = look s.Head 1 s.Tail []
        match c with
        | 1 -> s
        | _ -> lookAndSay s (c-1)

    [<TestCase("211", 1, ExpectedResult = "1221")>]
    [<TestCase("1", 1, ExpectedResult = "11")>]
    [<TestCase("11", 1, ExpectedResult = "21")>]
    [<TestCase("21", 1, ExpectedResult = "1211")>]
    [<TestCase("1211", 1, ExpectedResult = "111221")>]
    [<TestCase("111221", 1, ExpectedResult = "312211")>]
    [<TestCase("1", 5, ExpectedResult = "312211")>]
    member __.``Look and See Examples `` input iterations =
        lookAndSay (strToList input) iterations |> listToStr

    [<TestCase("211", 1, ExpectedResult = 4)>]
    [<TestCase("1", 1, ExpectedResult = 2)>]
    [<TestCase("11", 1, ExpectedResult = 2)>]
    [<TestCase("21", 1, ExpectedResult = 4)>]
    [<TestCase("1211", 1, ExpectedResult = 6)>]
    [<TestCase("111221", 1, ExpectedResult = 6)>]
    [<TestCase("1", 5, ExpectedResult = 6)>]
    [<TestCase("3113322113", 40, ExpectedResult = 329356)>]
    member __.``Part 1 `` input iterations =
        let result = lookAndSay (strToList input) iterations
        result.Length

    [<TestCase("3113322113", 50, ExpectedResult = 4666278)>]
    member __.``Part 2 `` input iterations =
        let result = lookAndSay (strToList input) iterations
        result.Length
