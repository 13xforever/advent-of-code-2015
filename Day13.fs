namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type Person = string
type Pair = Person * Person
type Link = Pair * int
type LinkLookup = Map<Pair, int>

type ``Day 13: Knights of the Dinner Table``() = 

    let toLink (l: string): Link =
        let valueSeparator = [| ' '; '\t'; '.' |]
        match l.Split(valueSeparator, StringSplitOptions.RemoveEmptyEntries) with
        | [| p1; _; m; w; _; _; _; _; _; _; p2 |] -> let ww = match m with
                                                              | "lose" -> -(int w)
                                                              | "gain" -> int w
                                                              | _ -> failwithf "Unknown modifier %s" w
                                                     ((p1, p2), ww)
        | _ -> failwithf "Invalid input line format: %s" l

    let parse (s: string) =
        let lineSeparator = [| '\r'; '\n' |]
        let oneWayLinks = s.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries)
                          |> Seq.ofArray
                          |> Seq.map toLink
                          |> List.ofSeq
        let pairs = oneWayLinks |> List.map fst
        let oneWayLookup = oneWayLinks |> Map.ofList
        let rec buildTwoWayLinks (result: LinkLookup) (lookup: LinkLookup) (lst: Pair list) =
            match lst with
            | [] -> result
            | h::t -> if (result.ContainsKey h) then
                        buildTwoWayLinks result lookup t
                      else
                        let revLnk = (snd h, fst h)
                        let w = lookup.[h] + lookup.[revLnk]
                        buildTwoWayLinks (result.Add(h, w).Add(revLnk, w)) lookup t
        buildTwoWayLinks Map.empty oneWayLookup pairs

    let getPersonList (m: LinkLookup) =
        Map.toSeq m
        |> Seq.map (fst >> fst)
        |> Seq.distinct
        |> List.ofSeq

    let insertSelfInto (g: LinkLookup) =
        let persons = getPersonList g
        seq { for p in persons do
                yield ((p, "Self"), 0)
                yield (("Self", p), 0)
        }
        |> Seq.append (Map.toSeq g)
        |> Map.ofSeq

    let maxFlow (s: string) (insertSelf: bool) =
        let graph = parse s
        let graph = if insertSelf then
                        insertSelfInto graph
                    else
                        graph
        let persons = getPersonList graph
        let rec maxFlowRec (mx: int) (c: int) (m: LinkLookup) (chain: Person list) (l: Person list) =
            match l with
            | [p] -> let c = c + m.[(p, List.last chain)]
                     max mx c
            | _ -> seq { for p in l.Tail do
                           let ll = p::(List.except (Seq.singleton p) l.Tail)
                           yield maxFlowRec mx (c+m.[(l.Head, p)]) m (l.Head::chain) ll
                   } |> Seq.max
        seq { for p in persons do
                let l = p::(List.except (Seq.singleton p) persons)
                yield maxFlowRec Int32.MinValue 0 graph [] l
        } |> Seq.max

    [<TestCase("test", ExpectedResult = 330)>]
    [<TestCase("real", ExpectedResult = 733)>]
    member this.``Part 1 `` input =
        match input with
        | "real" -> maxFlow this.RealInput false
        | _ -> maxFlow this.TestInput false

    [<TestCase(ExpectedResult = 725)>]
    member this.``Part 2 ``() =
        maxFlow this.RealInput true

    member __.TestInput = """
Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.
"""

    member __.RealInput = """
Alice would gain 2 happiness units by sitting next to Bob.
Alice would gain 26 happiness units by sitting next to Carol.
Alice would lose 82 happiness units by sitting next to David.
Alice would lose 75 happiness units by sitting next to Eric.
Alice would gain 42 happiness units by sitting next to Frank.
Alice would gain 38 happiness units by sitting next to George.
Alice would gain 39 happiness units by sitting next to Mallory.
Bob would gain 40 happiness units by sitting next to Alice.
Bob would lose 61 happiness units by sitting next to Carol.
Bob would lose 15 happiness units by sitting next to David.
Bob would gain 63 happiness units by sitting next to Eric.
Bob would gain 41 happiness units by sitting next to Frank.
Bob would gain 30 happiness units by sitting next to George.
Bob would gain 87 happiness units by sitting next to Mallory.
Carol would lose 35 happiness units by sitting next to Alice.
Carol would lose 99 happiness units by sitting next to Bob.
Carol would lose 51 happiness units by sitting next to David.
Carol would gain 95 happiness units by sitting next to Eric.
Carol would gain 90 happiness units by sitting next to Frank.
Carol would lose 16 happiness units by sitting next to George.
Carol would gain 94 happiness units by sitting next to Mallory.
David would gain 36 happiness units by sitting next to Alice.
David would lose 18 happiness units by sitting next to Bob.
David would lose 65 happiness units by sitting next to Carol.
David would lose 18 happiness units by sitting next to Eric.
David would lose 22 happiness units by sitting next to Frank.
David would gain 2 happiness units by sitting next to George.
David would gain 42 happiness units by sitting next to Mallory.
Eric would lose 65 happiness units by sitting next to Alice.
Eric would gain 24 happiness units by sitting next to Bob.
Eric would gain 100 happiness units by sitting next to Carol.
Eric would gain 51 happiness units by sitting next to David.
Eric would gain 21 happiness units by sitting next to Frank.
Eric would gain 55 happiness units by sitting next to George.
Eric would lose 44 happiness units by sitting next to Mallory.
Frank would lose 48 happiness units by sitting next to Alice.
Frank would gain 91 happiness units by sitting next to Bob.
Frank would gain 8 happiness units by sitting next to Carol.
Frank would lose 66 happiness units by sitting next to David.
Frank would gain 97 happiness units by sitting next to Eric.
Frank would lose 9 happiness units by sitting next to George.
Frank would lose 92 happiness units by sitting next to Mallory.
George would lose 44 happiness units by sitting next to Alice.
George would lose 25 happiness units by sitting next to Bob.
George would gain 17 happiness units by sitting next to Carol.
George would gain 92 happiness units by sitting next to David.
George would lose 92 happiness units by sitting next to Eric.
George would gain 18 happiness units by sitting next to Frank.
George would gain 97 happiness units by sitting next to Mallory.
Mallory would gain 92 happiness units by sitting next to Alice.
Mallory would lose 96 happiness units by sitting next to Bob.
Mallory would lose 51 happiness units by sitting next to Carol.
Mallory would lose 81 happiness units by sitting next to David.
Mallory would gain 31 happiness units by sitting next to Eric.
Mallory would lose 73 happiness units by sitting next to Frank.
Mallory would lose 89 happiness units by sitting next to George.
"""