namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type Chk = int->int->bool

type ``Day 17: No Such Thing as Too Much``() = 

    let parse (input: string) =
        let lineSeparator = [| '\r'; '\n' |]
        input.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries)
        |> Seq.ofArray
        |> Seq.map int
        |> List.ofSeq

    let recombine (l: 'a list) (v: 'a) =
        let result = List.except v l
        let diff = l.Length - result.Length
        if diff = 1 then
            result
        else
            seq { for i=1 to diff do yield i }
            |> List.ofSeq
            |> List.fold (fun acc _ -> v::acc) result

    let alwaysAcceptable (_: int) (_: int) = true
    let minAcceptable (cur: int) (min: int) = cur <= min

    let neverReset (_: int) (_: int) = false
    let minReset (cur: int) (min: int) = cur < min

    let alwaysInc (_: int) (_: int) = true
    let minInc (cur: int) (min: int) = cur = min

    let countCombinations (input: string) (target: int) (f: Chk) (r: Chk) (a: Chk) =
        let rec countCombinations (combos: int) (target: int) (minLen: int) (f: Chk) (r: Chk) (a: Chk) (curVal: int) (curLen: int) (l: int list) =
            if (curVal > target) || (not (f curLen minLen)) then
                (combos, minLen)
            else
                match l with
                | [] -> if curVal = target then
                            if r curLen minLen then
                                (1, curLen)
                            else if a curLen minLen then
                                (combos+1, minLen)
                            else
                                (combos, minLen)
                        else
                            (combos, minLen)
                | h::t -> let (combos, minLen) = countCombinations combos target minLen f r a curVal curLen t
                          countCombinations combos target minLen f r a (curVal+h) (curLen+1) t
        let cups = parse input
        let (result, _) = countCombinations 0 target cups.Length f r a 0 0 cups
        result

    [<TestCase("test", 25, ExpectedResult = 4)>]
    [<TestCase("real", 150, ExpectedResult = 1638)>]
    member this.``Part 1 `` input target =
        match input with
        | "real" -> countCombinations this.RealInput target alwaysAcceptable neverReset alwaysInc
        | _ -> countCombinations this.TestInput target alwaysAcceptable neverReset alwaysInc

    [<TestCase("test", 25, ExpectedResult = 3)>]
    [<TestCase("real", 150, ExpectedResult = 17)>]
    member this.``Part 2 `` input target =
        match input with
        | "real" -> countCombinations this.RealInput target minAcceptable minReset minInc
        | _ -> countCombinations this.TestInput target minAcceptable minReset minInc

    member __.TestInput = """
20
15
10
5
5
"""

    member __.RealInput = """
43
3
4
10
21
44
4
6
47
41
34
17
17
44
36
31
46
9
27
38
"""