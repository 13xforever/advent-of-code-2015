﻿namespace AdventOfCode2015.Tests
open System
open NUnit.Framework

type Ingredient = {
    name: string
    cap: int
    dur: int
    flv: int
    tex: int
    cal: int
}
type CookiePart = Ingredient * int
type Cookie = CookiePart list

type ``Day 15: Science for Hungry People``() = 

    let defaultIngredient: Ingredient = { name = null; cap = 0; dur = 0; flv = 0; tex = 0; cal = 0 }

    let getScore (c: Cookie) =
        c |> List.fold (fun acc p -> let (i, v) = p
                                     { name = null
                                       cap = acc.cap + i.cap*v
                                       dur = acc.dur + i.dur*v
                                       flv = acc.flv + i.flv*v
                                       tex = acc.tex + i.tex*v
                                       cal = acc.cal + i.cal*v
                                     }) defaultIngredient
        |> (fun c -> ((max 0 c.cap) * (max 0 c.dur) * (max 0 c.flv) * (max 0 c.tex), c.cal))
    
    let parseIngredient (l: string) =
        let valueSeparator = [| ' '; ','; ':' |]
        match l.Split(valueSeparator, StringSplitOptions.RemoveEmptyEntries) with
        | [| n; _; cap; _; dur; _; flv; _; tex; _; cal |] -> { name = n; cap = int cap; dur = int dur; flv = int flv; tex = int tex; cal = int cal }
        | _ -> failwithf "Invalid ingredient formant: %s" l
    
    let parse (s: string) =
        let lineSeparator = [| '\r'; '\n' |]
        s.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries)
        |> List.ofArray
        |> List.map parseIngredient

    let rec genBestCookie (best: int * Cookie) (cc: int -> bool) (vol: int) (c: Cookie) (l: Ingredient list) =
        match l with
        | [] -> let (bs, _) = best
                let (cs, cal) = getScore c
                if ((cc cal) && (cs > bs)) then (cs, c) else best
        | h::t -> match vol with
                  | 0 -> genBestCookie best cc vol c []
                  | _ -> seq { for v = 0 to vol do
                                if (v = 0) then
                                    yield genBestCookie best cc vol c t
                                else
                                    yield genBestCookie best cc (vol-v) ((h, v)::c) t }
                         |> Seq.maxBy fst

    let bestCookie (input: string) (cc: int -> bool) =
        let ingredients = parse input
        let (score, _) = genBestCookie (0, []) cc 100 [] ingredients
        score

    [<TestCase("test", ExpectedResult = 62842880)>]
    [<TestCase("real", ExpectedResult = 222870)>]
    member this.``Part 1 `` input =
        match input with
        | "test" -> bestCookie this.TestInput (fun _ -> true)
        | _ -> bestCookie this.RealInput (fun _ -> true)

    [<TestCase("test", ExpectedResult = 57600000)>]
    [<TestCase("real", ExpectedResult = 117936)>]
    member this.``Part 2 `` input =
        match input with
        | "test" -> bestCookie this.TestInput (fun cal -> cal = 500)
        | _ -> bestCookie this.RealInput (fun cal -> cal = 500)

    member __.TestInput = """
Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3
"""

    member __.RealInput = """
Sugar: capacity 3, durability 0, flavor 0, texture -3, calories 2
Sprinkles: capacity -3, durability 3, flavor 0, texture 0, calories 9
Candy: capacity -1, durability 0, flavor 4, texture 0, calories 1
Chocolate: capacity 0, durability 0, flavor -2, texture 2, calories 8
"""