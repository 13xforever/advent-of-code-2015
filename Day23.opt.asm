	jio ax, .l1
	; ax = (((ax+2)*27+2)*3+2)*27+1
	; ax = ax*2187+4591
	; ax = 0
    mov ax, 4591
    jmp .loop
.l1:
	; (((((((ax*3+2)*3+2)*9+2)*3+1)*3+1)*3+2)*3+1)*9+1
	; ax*3*3*9*3*3*3*3*9 + ((((((2*3+2)*9+2)*3+1)*3+1)*3+2)*3+1)*9+1
	; ax*59049 + 54334
	; ax = 1
	mov ax, 113383
.loop:
	jio ax, .e
    inc bx
    jie ax, .even
.odd:
    mul ax, 3
    inc ax
    jmp .loop
.even:
	div ax, 2
	jmp .loop
.e: