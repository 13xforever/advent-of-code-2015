namespace AdventOfCode2015.Tests
open NUnit.Framework

type ``Day 20: Infinite Elves and Infinite Houses``() = 

    let findDividers (n: int) (known: Map<int, Set<int>>) =
        let rec findRec (d: int) (result: Set<int>) =
            let d = d + 1
            if d*2 > n then
                result
            else if n%d = 0 then
                let gd = n/d
                let result = result.Add(d).Add(gd)
                if known.ContainsKey gd then
                    known.[gd]
                    |> Set.map (fun e -> e*d)
                    |> Set.union known.[gd]
                    |> Set.union result
                else
                    findRec d result
            else
                findRec d result
        if n = 1 then
            Set.singleton 1
        else
            findRec 1 (Set.ofList [1; n])

    let findN (n: int) =
        let (idx,  _) = seq { let mutable known = Map.empty
                              for i=1 to n do
                                let dividers = findDividers i known
                                known <- known.Add(i, dividers)
                                yield (i, dividers) }
                        |> Seq.where (fun (_, dl) -> (Seq.sum dl) >= n)
                        |> Seq.head
        idx

    let genSeq (s: int) (result: Map<int, int>) =
        let mutable r = result
        for i in s .. s .. s*50 do
            r <- if r.ContainsKey i then
                    r.Add(i, r.[i] + s)
                 else
                    r.Add(i, s)
        r
    
    let findM (m: int) =
        seq { let mutable res = Map.empty
              for i=1 to m do
                    if res.ContainsKey i then
                        if res.[i]+i >= m then
                            yield i
                        else
                            res <- genSeq i res
                    else
                        if i >= m then
                            yield i
                        else
                            res <- genSeq i res }
        |> Seq.head

    [<TestCase(10, ExpectedResult = 1)>]
    [<TestCase(60, ExpectedResult = 4)>]
    [<TestCase(80, ExpectedResult = 6)>]
    [<TestCase(130, ExpectedResult = 8)>]
    [<TestCase(29_000_000, ExpectedResult = 665280)>]
    member __.``Part 1 `` input =
        findN (input / 10)

    [<TestCase(29_000_000, ExpectedResult = 705600)>]
    member __.``Part 2 `` input =
        findM (input / 11)
